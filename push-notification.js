// region Variables

var platform = Ti.Platform.osname;
var android = 'android';
var iOS = (Ti.Platform.osname == 'ipad' || Ti.Platform.osname == 'iphone');

// endregion

// region Functions

var PushNotification = {

    // Enable Push Notifications
    enablePushNotification: function (yes, channel, pushCallback) {

        // Cancel Push Notification
        if (!yes || channel == undefined || pushCallback == undefined) {
            return;
        }

        // Enable Push Notifications For Android
        if (platform == android) {
            var CloudPush = require('ti.cloudpush');
            CloudPush.enabled = true;

            // Request Device Token And Subscribe To Alert Channel
            CloudPush.retrieveDeviceToken({
                success: function deviceTokenSuccess(e) {
                    Ti.App.Properties.setString('pushToken', e.deviceToken);
                    if (!Titanium.App.Properties.getBool('tokenSubscribed', false)) {
                        PushNotification.subscribeToChannel(e.deviceToken, channel);
                    }
                },
                error: function deviceTokenError(e) {
                    alert('Failed to register for push! ' + e.error);
                }
            });

            // Handle Received Push Notifications
            CloudPush.addEventListener('callback', function (evt) {
                if (typeof pushCallback === 'function') {
                    pushCallback(evt);
                }
            });
        }

        // Enable Push Notifications For iOS
        else if (iOS) {

            // Save Device Token
            function tokenReceivedIOS(e) {
                Ti.App.Properties.setString('pushToken', e.deviceToken);
                if (!Titanium.App.Properties.getBool('tokenSubscribed', false)) {
                    PushNotification.subscribeToChannel(e.deviceToken, channel);
                }
            }

            // Handle Registration Errors
            function errorCallbackPush(e) {
                Ti.App.Properties.setString('pushToken', '');
            }

            // Register For Push Notifications Based On iOS Version
            if (Ti.Platform.name == 'iPhone OS' && parseInt(Ti.Platform.version.split('.')[0]) >= 8) {

                // Register For Push Notification
                function registerForPush() {
                    Ti.Network.registerForPushNotifications({
                        success: tokenReceivedIOS,
                        error: errorCallbackPush,
                        callback: pushCallback
                    });

                    // Remove Event Listener Once Registered For Push Notifications
                    Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
                }

                // Wait For User Settings To Be Registered Before Registering For Push Notifications
                Ti.App.iOS.addEventListener('usernotificationsettings', registerForPush);

                // Register Notifications Types To use
                Ti.App.iOS.registerUserNotificationSettings({
                    types: [
                        Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
                        Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
                        Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
                    ]
                });

            } else {

                // For iOS 8 And Higher, Register For Push Notification
                var registerForPushIOS = function (e) {
                    Titanium.Network.registerForPushNotifications({
                        types: [
                            Titanium.Network.NOTIFICATION_TYPE_BADGE,
                            Titanium.Network.NOTIFICATION_TYPE_ALERT,
                            Titanium.Network.NOTIFICATION_TYPE_SOUND
                        ],
                        success: tokenReceivedIOS,
                        error: errorCallbackPush,
                        callback: pushCallback
                    });
                };

                // Call Register For Push Notification Function
                registerForPushIOS();

            }
        }

    },

    // Set Badge Count
    setBadgeCount: function (count) {

        if (OS_IOS) {
            if (count != '') {
                Ti.UI.iOS.appBadge = count;
            }
        }

    },

    // Subscribe To Alert Channel
    subscribeToChannel: function (token, channel, callback) {

        var Cloud = require('ti.cloud');
        Cloud.debug = true;

        Cloud.PushNotifications.subscribeToken({
            device_token: token,
            type: iOS ? 'ios' : 'android',
            channel: channel
        }, function (e) {
            if (e.success && callback) {
                callback();
            }
        });

    }

};

module.exports = PushNotification;

// endregion

// region Sample Usage

/*

 var PushNotification = require('PushNotification');
 PushNotification.enablePushNotification(true, 'pockyt_consumer', function (pushNotification) {
 if (OS_ANDROID) {
 var androidPushNotification = JSON.parse(pushNotification['payload'])['android'];
 console.error(androidPushNotification);
 alert(androidPushNotification['alert']);
 }
 else if (OS_IOS) {
 alert(pushNotification['data']['alert']);
 }
 });

 */

// endregion
